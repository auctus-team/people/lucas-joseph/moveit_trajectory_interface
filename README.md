# Moveit_trajectory_interface

This package uses moveit functions to expose a moveit trajectory. By default moveit doesn't provide a trajetory as an object that can be called for any time step along a path. Instead it send a trajectory_msgs with waypoints specifying a target position, velocity and acceleration. For the control tools developped by Auctus this is an issue as a trajectory must be callable for any time step. 

This package uses an action server to get a trajectory_msgs planned by MoveIt and uses [toppra](https://github.com/hungpham2511/toppra) tools to define a trajectory that can be called for any time. This action server is called by the [moveit_simple_controller_manager_extended](https://gitlab.inria.fr/auctus-team/components/motion-planning/moveit_simple_controller_manager_extended) package.

The trajectory object is created inside the control (see [https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_control](https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_control) for example) and an `updateTrajectory()` function can be run periodically. You can specify the time increment of the trajectory with the `setTrajectoryTimeIncrement()`  function. A separate thread is launch to run the action server. 

The controller is used seamlessly inside moveit. Examples of how to define trajectories are presented in the `test` folder. 
