#pragma once

#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <moveit_trajectory_interface/TrajectoryAction.h>
#include <moveit_trajectory_interface/moveit_trajectory.hpp>

class TrajectoryAction
{
protected:

  ros::NodeHandle nh_;
  actionlib::SimpleActionServer<moveit_trajectory_interface::TrajectoryAction> as_; // NodeHandle instance must be created before this line. Otherwise strange error occurs.
  std::string action_name_;
  // create messages that are used to published feedback/result
  moveit_trajectory_interface::TrajectoryFeedback feedback_;
  moveit_trajectory_interface::TrajectoryResult result_;
  std::shared_ptr<MoveItTrajectory> trajectory;

public:

  TrajectoryAction(std::string name, std::shared_ptr<MoveItTrajectory> trajectory ) :
    as_(nh_, name, boost::bind(&TrajectoryAction::executeCB, this, _1), false),
    action_name_(name)
  {
    this->trajectory = trajectory;
    as_.start();
  }

  ~TrajectoryAction(void)
  {
  }

  void executeCB(const moveit_trajectory_interface::TrajectoryGoalConstPtr &goal)
  {
    ros::Rate r(1);
    bool success = true;
    success = trajectory->computeNewTrajectory(goal->path,goal->max_velocity_scaling_factor,goal->max_acceleration_scaling_factor);

    while (trajectory->getTimeProgress() <1.0)
    {
      // check that preempt (meaning cancelled) has not been requested by the client
      if (as_.isPreemptRequested() || !ros::ok())
      {
          ROS_ERROR("%s: Preempted", action_name_.c_str());
          // set the action state to preempted
          as_.setPreempted();
          trajectory->stop();
          success = false;
      }
      feedback_.time_ratio = trajectory->getTimeProgress() ;
      // publish the feedback
      as_.publishFeedback(feedback_);
    }
   

    if(success)
    {
      as_.setSucceeded(result_);
    }
  }
};
